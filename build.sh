#!/usr/bin/env bash
# exit on error
set -o errexit

pip install -r requirements.txt
DJANGO_SUPERUSER_PASSWORD=1234 python manage.py createsuperuser --no-input --username=kida  --email=joe@example.com

python manage.py collectstatic --no-input
python manage.py migrate
